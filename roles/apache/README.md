# STARTX services : Apache role

Read the [startxfr.services.apache role documentation](https://startx-ansible-services.readthedocs.io/en/latest/roles/apache/)
for more information on how to use [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services) apache role.
