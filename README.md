# STARTX Services Ansible collection

The purpose of this collection is to manage the creation-deletion of various services
into an linux operating-system.

Refer to [latest documentation for the `startxfr.services` ansible collection](https://startx-ansible-services.readthedocs.io)
for more informations on how to configure and use this top level role.

## Availables roles

- [chrony role](https://startx-ansible-services.readthedocs.io/en/latest/roles/chrony)
- [apache role](https://startx-ansible-services.readthedocs.io/en/latest/roles/apache)
- [mariadb role](https://startx-ansible-services.readthedocs.io/en/latest/roles/mariadb)
- [vault role](https://startx-ansible-services.readthedocs.io/en/latest/roles/vault)

## History

Full history for this collection is available on the [startx services collection history](https://startx-ansible-services.readthedocs.io/en/latest/history) page.
