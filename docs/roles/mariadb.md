# Services : Mariadb role

The purpose of this role is to install, configure and enable the mariadb service.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the [startx services collection](https://galaxy.ansible.com/startxfr/services) with `ansible-galaxy collection install startxfr.services`

## Role Variables

| Key                    | Default                          | Description                |
| ---------------------- | -------------------------------- | -------------------------- |
| ss_mariadb_action      | create                           | The action to perform      |
| ss_mariadb_release     | latest                           | mariadb version to install |
| ss_mariadb_config_file | /etc/my.cnf.d/mariadb-server.cnf | mariadb configuration file |

## Dependencies

Depend only on `ansible.builtin`

## Example playbooks

### Install mariadb playbook

Install mariadb service default version.

```yaml
- name: Install mariadb service
  hosts: localhost
  roles:
    - role: startxfr.services.mariadb
```

### Install mariadb version 10.5.13" playbook

Install mariadb service 10.5.13" version.

```yaml
- name: Install mariadb service in version 10.5.13"
  hosts: localhost
  roles:
    - role: startxfr.services.mariadb
      ss_mariadb_release: "10.5.13"
```

### Uninstall mariadb playbook

Uninstall mariadb service.

```yaml
- name: Uninstall mariadb service
  hosts: localhost
  roles:
    - role: startxfr.services.mariadb
      ss_mariadb_action: "delete"
```
