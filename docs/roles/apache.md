# Services : Apache role

The purpose of this role is to install, configure and enable the apache service.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the [startx services collection](https://galaxy.ansible.com/startxfr/services) with `ansible-galaxy collection install startxfr.services`

## Role Variables

| Key                   | Default                        | Description               |
| --------------------- | ------------------------------ | ------------------------- |
| ss_apache_action      | create                         | The action to perform     |
| ss_apache_release     | latest                         | apache version to install |
| ss_apache_config_file | /etc/httpd/conf.d/default.conf | apache configuration file |

## Dependencies

Depend only on `ansible.builtin`

## Example playbooks

### Install apache playbook

Install apache service default version.

```yaml
- name: Install apache service
  hosts: localhost
  roles:
    - role: startxfr.services.apache
```

### Install apache version 2.4.52" playbook

Install apache service 2.4.52" version.

```yaml
- name: Install apache service in version 2.4.52"
  hosts: localhost
  roles:
    - role: startxfr.services.apache
      ss_apache_release: "2.4.52"
```

### Uninstall apache playbook

Uninstall apache service.

```yaml
- name: Uninstall apache service
  hosts: localhost
  roles:
    - role: startxfr.services.apache
      ss_apache_action: "delete"
```
