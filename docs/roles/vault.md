# Services : Vault role

The purpose of this role is to install, configure and enable the vault service.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the [startx services collection](https://galaxy.ansible.com/startxfr/services) with `ansible-galaxy collection install startxfr.services`

## Role Variables

| Key                  | Default                | Description              |
| -------------------- | ---------------------- | ------------------------ |
| ss_vault_action      | create                 | The action to perform    |
| ss_vault_release     | latest                 | vault version to install |
| ss_vault_config_file | /etc/vault.d/vault.hcl | vault configuration file |

## Dependencies

Depend on `ansible.builtin` and `startxfr.services.vault`

## Example playbooks

### Install vault playbook

Install vault service default version.

```yaml
- name: Install vault service
  hosts: localhost
  roles:
    - role: startxfr.services.vault
```

### Install vault version 1.9.3 playbook

Install vault service 1.9.3 version.

```yaml
- name: Install vault service in version 1.9.3
  hosts: localhost
  roles:
    - role: startxfr.services.vault
      ss_vault_release: "1.9.3
```

### Uninstall vault playbook

Uninstall vault service.

```yaml
- name: Uninstall vault service
  hosts: localhost
  roles:
    - role: startxfr.services.vault
      ss_vault_action: "delete"
```
